package th.go.rd.rdzdeploymentservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import th.go.rd.rdzdeploymentservice.entity.MasterStatus;

@Repository
public interface MasterStatusRepository extends JpaRepository<MasterStatus,Integer>{
	MasterStatus findByStatus(String status);
}
