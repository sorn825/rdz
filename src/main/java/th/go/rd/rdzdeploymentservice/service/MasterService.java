package th.go.rd.rdzdeploymentservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.rd.rdzdeploymentservice.entity.MasterStatus;
import th.go.rd.rdzdeploymentservice.repository.MasterStatusRepository;

@Service
public class MasterService {

	@Autowired
	MasterStatusRepository masterStatusRepository;

	public List<MasterStatus> getStatus() {
		return masterStatusRepository.findAll();
	}
}
