FROM sorn.com:8082/openjdk:8u111-jdk-alpine
ARG program_name
LABEL Author="sanprasirt@pccth.com"
VOLUME /tmp
ADD /target/${program_name}.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]